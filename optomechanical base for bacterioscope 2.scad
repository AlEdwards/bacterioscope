

//camera box just to see where lens goes
//translate([200,200,50]) rotate([0,0,45]) cube([30,30,30]); 
///////////

//render camera mount and baseplate and example blockers

translate([200,220,0]){     hqmount();   }

baseplate();
    
translate([0,-20,5]) blocker(wide=45, high=100);

////////////////////////

    module hqmount(camh=100){
        
        difference(){
            
union(){
    //hq camera mount add here
        
translate([40,20,40]) rotate([0,0,45]) cube([6,40,camh-50]);
translate([23,28,40]) rotate([0,0,45]) cube([10,4,camh-50]);//mount for mount
        
translate([29,37,camh]) rotate([90,0,-45]) boardmount2(pi2=false);

//camera mount mount onto 2020 extrusion
difference(){
hull(){
 rotate([0,0,0]) cube([40,4,30]);

translate([40,20,20]) rotate([0,0,45]) cube([4,40,30]); 
}
//set of holes to bolt onto v-slot using t-nuts
translate([10,50,10])rotate([90,0,0])cylinder(d=5.5,h=100);
translate([30,50,10])rotate([90,0,0])cylinder(d=5.5,h=100);
translate([10,55,10])rotate([90,0,0])cylinder(d=11,h=50);
translate([30,55,10])rotate([90,0,0])cylinder(d=11,h=50);

}

//translate([200,220,00]) rotate([0,0,0]) cube([40,4,40]);

}

translate([42,22,30]) rotate([0,0,45]) translate([1,-50,-50]) cube([4,180,180]);//mount for mount
}
}



//////////////////////////CAMERA MOUNT HERE- TWO MODULES
//camera mount parameters varibales and spacing
lugy=10;
lugx=20;

$fn=12;
    // HQ holes are 4mm from edge of 38mm square; i.e. 38/2-4 mm from centrepoint 19-4=15mm 
HQmount=15;

//LUGS for camera v2 board
module lugs(){
difference(){
hull(){
translate([10.4,0,0]) cylinder(d=4,h=10);
translate([20,0,0]) cylinder(d=8,h=4);
}
translate([10.4,0,-50]) cylinder(d=2,h=100);
}
}
 
//////////////////////////
////////////CAMERA MOUNT ITSELF

module boardmount2(depth=4,pi2=true) {
union(){
//FIRST LUGS FOR CAMERA v2 BOARD has 2.2mm mounting holes; can comment out for HQ camera
if(pi2){

lugs();
mirror([1,0,0]){
    lugs();
}
}
//THEN PLATE FOR MOUNTING
difference(){
union(){
    //camera mount base sheet (can be various shapes)
hull(){
translate([0,0,0]) cylinder(d=56,h=3);
translate([0,-10,0]) cylinder(d=56,h=3);

}
translate([0,0,0]) cylinder(d=46,h=5);    

//add lugs for HQ camera    

translate([HQmount,HQmount,0]) cylinder(r=2,h=10);
translate([HQmount,-HQmount,0]) cylinder(r=2,h=10);
translate([-HQmount,HQmount,0]) cylinder(r=2,h=10);
translate([-HQmount,-HQmount,0]) cylinder(r=2,h=10);  

}


translate([-13,-13,-50]) cube([26,26,100]); //space for camera

    //ADD 2mm HOLES FOR HQ camera- PCB has 2.5mm mounting holes 
translate([HQmount,HQmount,-50]) cylinder(d=2,h=100);
translate([HQmount,-HQmount,-50]) cylinder(d=2,h=100);
translate([-HQmount,HQmount,-50]) cylinder(d=2,h=100);
translate([-HQmount,-HQmount,-50]) cylinder(d=2,h=100);

}
}
}

/////////////////////////////////////



//baseplate
module baseplate(sq=200,grid=20){

//edge of baseplate for rigidity and attaching 2020 extrusion
difference(){
translate([0,200,0]) cube([200,4.2,20]);
//holes to bolt onto v-slot extrusion
    for (
        h= [grid/2:grid:sq]) 
translate([h,sq+50,10])rotate([90,0,0])cylinder(d=5.5,h=100);

}
difference(){    
cube([sq,sq,4]);
//needs holes to mount onto t-nuts
    for (
        i= [grid/2:grid:sq],
        j= [grid/2:grid:sq]) {
translate([i,j,-50]) cylinder(h=100,d=4);
translate([i,j,-5]) cylinder(h=8,d=10);

        }
    }
}
/////////////////////////


//blocker customise using wide value

module blocker(wide=85, bolt=4, slot=8, high=200) {
    union(){
    translate([0,0,0]) cube([wide,3,high]);
        hull(){
    translate([wide/2,0,0]) cube([2,2,high]);
    translate([wide/2,0,0]) cube([2,slot+12,2]);
            
    }
    difference(){
translate([0,0,0]) cube([wide,slot+12,5]);        
    for (p=[slot: 3*slot: wide-slot]) {
        hull(){
translate([p,(slot+12)/2,-5]) cylinder(h=20,d=bolt);
translate([p+slot,(slot+12)/2,-5]) cylinder(h=20,d=bolt);
        }
        hull(){
translate([(p+2*slot),6,-5]) cylinder(h=20,d=bolt);
translate([(p+2*slot),slot+6,-5]) cylinder(h=20,d=bolt);
        }
    }
    }
    
}
}
//testing render:
//translate([-90,0,10]) blocker();
