///////BLOCKER MODULES
//blocker customise using wide and high values
/////////////////////////////////

//PLENTY TO CUSTOMISE There is additional blocker

//render
blocker(wide=65, bolt=4, slot=8, high=150);
/////////////////////////////


module blocker(wide=85, bolt=4, slot=8, high=200) {
    union(){
    translate([0,0,0]) cube([wide,2,high]);//main plate
        hull(){ //spine
    translate([wide/2,0,0]) cube([2,2,high]);
    translate([wide/2,0,0]) cube([2,slot+10,2]);
            
    }
    difference(){
translate([0,0,0]) 
hull(){
        cube([wide,1,5]);
translate([12,14+slot/2,0])        cube([wide-24,1,3]);       
} 
        //base plate with tapere        
    for (p=[slot: 3*slot: wide-slot]) {
        hull(){
translate([p,(slot+12)/2,-5]) cylinder(h=20,d=bolt);
translate([p+slot,(slot+12)/2,-5]) cylinder(h=20,d=bolt);
        }
        hull(){
translate([(p+2*slot),6,-5]) cylinder(h=20,d=bolt);
translate([(p+2*slot),slot+6,-5]) cylinder(h=20,d=bolt);
        }
    }
    }
    
}
}
