
Plan for efficient repo creation
Use existing repo

fork from gitlab using ssh
'get' all files 
checkout working fork

add files in folders to local fork
use git to add files properly



Plan for folders:
folders efficiently curated to contain elements as outlined in readme DONE

openscad and stl in separate folders- easier to find DONE

break apart master openscad into different components- easier to render into STL without more advanced openscad coding skills
DONE



# Instructions
====

# How to configure and build #

### 1 Configuration
Choose your camera, light source, box container, sample holder
Decide if light source requires switching controlled by the Raspberry Pi using python scripts. If so, an external relay module is required to switch the power supply to the LED strip. We used xxx module but many options are available.
Light source switching helps with longer timecourse experiments where continual exposure of sample to light would be detrimental. In many cases this is not necessary.
Raspberry Pi model will depend on imaging requirements. For high framerate high resolution video or timelapse, a more powerful model may be required. For most purposes a model 3 is sufficient.

### 2 Gather hardware
Full details of hardware required listed within BOM documents (Bill of Materials).
Screws, nuts, aluminium 2020 extrusion, are all easily available. Alternative options can be substituted where required.
Plastic box model supplied; any other suitably sized box with lid can be substituted. If interior is not black, it can be lined with black paper or fabric, or painted matt black.
If touchscreen is required, cut space for touchscreen and Raspberry Pi box mount through side wall of box. Raspberry Pi fits at opposite corner from camera.

Raspberry Pi requires SD card formatted with Raspbian operating system, with Python 3 installed to run camera scripts.

### 3 3D print parts
All parts are designed to be printed without supports with one flat face.
Large flat parts including blockers and base plate should be printed flat.
Camera mount needs *rotating 45 degrees for printing* so that the **back of camera mount plate** is flat on print bed to avoid needing supports.
List of 3D printed parts required included within BOM documents.

# Typical print parameters:
Designed for standard generic fused filament deposition printer. Printed well on Ender 3, Prusa i3 MK3, D-bot and Hypercube Evolution.
Black PLA generic
Conventional/generic/default print conditions
20% infill
0.2mm or 0.3mm layer height

Base plate can be printed in several sections bolted together via extrusion to fit on bed, if base plate area greater than your print bed required.

### 5 Assemble

HAVE NOT YET ORDERED ASSEMBLY BUT THESE STEPS SHOULD BE ORDERED. ALSO PHOTOS WILL HELP. EACH STEP SHOULD BE CLEAR AND AVOID TECHNICAL LANGUAGE.
ORDER OF WIRING AND CABLE ATTACHMENT WILL MAKE BIG DIFFERENCE
ONCE CAMERA CONNECTED TO PI, will be harder to modify or change the blockers etc.

Attach LED strip to LED mount and wire up to power supply socket

Bolt camera mount onto 2020 extrusion using t-nuts slotted into the extrusion.
Camera mount should be at end of extrusion so it fits into corner of box.

Bolt 2020 extrusion with camera mount onto base plate.

Bolt LED mount onto baseplate in corner on same side as camera but opposite end of box.

Bolt sample mount in centre of baseplate

Bolt blockers onto baseplate on both sides of LED light source to block stray light from LED

Bolt blockers on either side of sample to cut glare from back of box behind sample.

Bolt sample frame blocker in just behind sample mount with frame showing position of samples.

***POSITIONS OF BLOCKERS WILL BE ACCORDING TO CLEAR DIAGRAM***

Attach camera to mount using xxxx screws. These can either be threaded into the 3D printed plastic holes, or bolted through to nuts.

Fit camera ribbon cable to Raspberry Pi.

Insert SD card into Raspberry Pi. Ideally check Raspbian operating system is working prior to full installation.

Attach Raspberry Pi to case and box
If touchscreen added, mount and touchscreen is screwed through wall box to Pi Case.

Attach camera cable to camera 



### 5 Check and test

# Check camera and Raspberry Pi
Switch on Raspberry Pi.
Use camera preview script to check image.
# Focus camera on sample site.
Camera can be easily focussed using a paper sheet with clear sharp text printed (e.g. black text printout on white paper) held in the sample plane.

# Check illumination 
LED illumination of white paper in sample location should give uniform intensity indicating even illumination.

When test sample is placed in holder, check for glare from around sample. Brightest object in the image ideally should be a reflection from sample frame, or from control diffuser added alongside sample (e.g. white tape or paper).




