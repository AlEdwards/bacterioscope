/////////////////////////////
///CAMERA MOUNTING TO 2020 extrusion BAR//////
/////////////////////////////

////RENDER HERE FOR PRINTING:
rotate([270,0,0]) rotate([0,0,45]) hqmount();
////////////NOTE: can switch pi2=true in hqmount module if you want to mount v2 camera instead of HQ camera



    module hqmount(camh=100){
        
        difference(){
            
union(){
    //hq camera mount add here
        
translate([40,20,40]) rotate([0,0,45]) cube([6,40,camh-50]);
translate([23,28,40]) rotate([0,0,45]) cube([10,4,camh-50]);//mount for mount
        
translate([29,37,camh]) rotate([90,0,-45]) boardmount2(pi2=false); ///CAN CHANGE TO GIVE LUGS FOR v2 camera

//camera mount mount onto 2020 extrusion
difference(){
hull(){
 rotate([0,0,0]) cube([40,4,30]);

translate([40,20,20]) rotate([0,0,45]) cube([4,40,30]); 
}
//set of holes to bolt onto v-slot using t-nuts
translate([10,50,10])rotate([90,0,0])cylinder(d=5.5,h=100);//bolt hole
translate([30,50,10])rotate([90,0,0])cylinder(d=5.5,h=100);//bolt hole
translate([10,55,10])rotate([90,0,0])cylinder(d2=9,d1=50,h=50);//access
translate([0,55,0])rotate([90,0,0])cylinder(d2=20,d1=80,h=50);//trying to help remove print overhang
translate([30,55,10])rotate([90,0,0])cylinder(d2=9,d1=30,h=50);//access

}


}

translate([42,22,30]) rotate([0,0,45]) translate([1,-50,-50]) cube([4,180,180]);//mount for camera mount
}
}
//end camera mount module- but needs next two modules- lugs and mount
////////////////////////////////////////


//////////////////////////CAMERA MOUNT continues- TWO MODULES
//camera mount parameters varibales and spacing within modules

//LUGS for camera v2 board
module lugs(){
$fn=12;
lugy=10;
lugx=20;
difference(){
hull(){
translate([10.4,0,0]) cylinder(d=4,h=10);
translate([20,0,0]) cylinder(d=8,h=4);
}
translate([10.4,0,-50]) cylinder(d=2,h=100);
}
}
 
//////////////////////////
////////////CAMERA MOUNT ITSELF
/////////////////CAN choose HQ or v2

module boardmount2(depth=4,pi2=true) {

HQmount=15;     // HQ holes are 4mm from edge of 38mm square; i.e. 38/2-4 mm from centrepoint 19-4=15mm 

union(){
//FIRST LUGS FOR CAMERA v2 BOARD has 2.2mm mounting holes; can comment out for HQ camera
if(pi2){

lugs();
mirror([1,0,0]){
    lugs();
}
}
//THEN PLATE FOR MOUNTING
difference(){
union(){
    //camera mount base sheet (can be various shapes)
hull(){
translate([0,0,0]) cylinder(d=56,h=3);
translate([0,-10,0]) cylinder(d=56,h=3);

}
translate([0,0,0]) cylinder(d=46,h=5);    

//add lugs for HQ camera    

translate([HQmount,HQmount,0]) cylinder(r=2,h=10);
translate([HQmount,-HQmount,0]) cylinder(r=2,h=10);
translate([-HQmount,HQmount,0]) cylinder(r=2,h=10);
translate([-HQmount,-HQmount,0]) cylinder(r=2,h=10);  

}


translate([-13,-13,-50]) cube([26,26,100]); //space for camera and cable

    //ADD 2mm HOLES FOR HQ camera- PCB has 2.5mm mounting holes 
translate([HQmount,HQmount,-50]) cylinder(d=2,h=100);
translate([HQmount,-HQmount,-50]) cylinder(d=2,h=100);
translate([-HQmount,HQmount,-50]) cylinder(d=2,h=100);
translate([-HQmount,-HQmount,-50]) cylinder(d=2,h=100);

}
}
}

/////////////////////////////////////

//END OF CAMERA MOUNT
