*hello*

**boo**

-bullet
 -bullet 
 
1. list
2. list



### Customisable optomechanical system for darkfield imaging of light scattering from cell suspensions ###


## Aim: ##

## Features: ##
1. Allows darkfield imaging to quantify light scattering by cell suspensions including bacteria and eukaryotic cells
2. Fully programmable high quality image aquisition with 12Mp HQ Raspberry Pi camera
3. Light source is 12v LED strip
4. Ideal for time-lapse imaging
5. Optimised for small sample volume using microfluidic cuvette, but can be configured for conventional lab plasticware
6. Compact footprint of x x y x z 
7. Open source hardware and uses simple scripting; compatible with wide range of imaging and analysis software 
8. Optional onboard touchscreen or remote wireless operation via smartphone/laptop/network

##This repository will contain the following:##
- License

- Bill of materials
- 3D printed component list and description
- 3D design files for printing
- 3D CAD codebase for design, re-design and customisation

- Configuration guide
- Assembly instructions
- Instructions for use
- Optimisation instructions (standardisation; how to maxmise signal and 
- Tutorial: getting started (do I want darkfield imaging? will it work for my application?)
- Tutorial: how to measure bacterial growth curve
- Tutorial: how to count bacteriophage by microfluidic
- Tutorial: how to measure platelet aggregation



====

### System and hardware

## Bill of materials

open office spreadsheet

# Hardware
- Plastic box: Black, h= mm l= mm w= mm 
- Aluminium extrusion
- Bolts and nuts

# 3D printed parts
- Raspberry Pi camera mount (v2 or HQ options)
- Base plate
- LED mount
- Raspberry Pi computer and touchscreen mount (3rd party option suggested for box mounting)
- Sample holder
- Set of blockers

# Electronics
- LED strip
- 12v power supply for LED strip
- optional relay control module for LED switching
- Raspberry Pi
- Raspberry Pi camera (and lens)

# Control software
- Python scripts for camera (and LED) operation
- Configuring Raspberry Pi basic guide (how to setup SD card and connect through wifi; camera configuration; getting your files) 

# Assays
-diffuser as control/standardisation
-microcapillary microfluidics how-to guide

====
## Configuration guide

# Light source
Aim: point source from one or more LED
12v white LED strip inexpensive, effective
OPTION: switched by Raspberry Pi, or manually operated

# Camera
Aim: single image with field-of-view large enough to image sample or panel of samples
Full control of image aquistion by Raspberry Pi using Python scripts (Pycam library)

OPTION: v2 camera or HQ camera
HQ camera offers higher resolution and more optics options with many different C and CS-mount lenses available, allowing wide angle for larger samples or macro/microscopy. 
HQ camera OPTIONS: can use different lens. Wide angle (6mm focal length) recommended by Raspberry Pi foundation works fine, with some distortion at edges
v2 camera is cheaper and harder to focus sharply, but still takes excellent images

## Optimisation guide
Explain how to move sample and blockers to eliminate glare and get sharpest image

====
