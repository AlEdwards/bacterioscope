

//////////////////BASEPLATE MODULE////////

///render here
baseplate(sql=200,sqw=140,grid=15); 
// can change various parameters including dimensions and grid spacing
//140 wide fits nicely in box with three sets of slots
//design note: the flat base is to aid print adhesion for first layer; cut through to insert countersunk M4 bolts from beneath


//baseplate
module baseplate(sql=200,sqw=140,grid=15){

//edge of baseplate for rigidity and attaching 2020 extrusion
union(){

difference(){
translate([0,sqw,0]) cube([sql,4.2,20]);
//holes to bolt onto v-slot extrusion
    for (
        h= [grid/2:grid:sql]) 
translate([h,sqw+50,10])rotate([90,0,0])cylinder(d=5.5,h=100);

}
difference(){    
cube([sql,sqw,5]); //MAIN PLATE
//needs holes to mount onto t-nuts
    for (
        i= [grid/2:grid:sql],
        j= [grid/2:3*grid:sqw]) {
            hull(){
translate([i,j,-50]) cylinder(h=100,d=4);
translate([i,j+2*grid,-50]) cylinder(h=100,d=4);   
            }
             hull(){
translate([i,j,-5]) cylinder(h=9,d=4);
translate([i,j+2*grid,-5]) cylinder(h=9,d=4);   
 //           }
 //           hull(){
translate([i,j,-6]) cylinder(h=7,d=8);
translate([i,j+2*grid,-6]) cylinder(h=7,d=8);   

            }
        }
    }
cube([sql,sqw,0.3]); //sub-plate to aid printing
} //end union of baseplate
}
///////////////////////// END OF BASE PLATE
