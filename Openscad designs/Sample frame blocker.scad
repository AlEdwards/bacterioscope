
//Hole blocker frame for cleaning up image next to sample
//aims to provide black frame to sample
//tapered hole hopes to reduce glare around edge of blocker
///////////////////////

//render:
translate([0,0,0]) holyblocker();
//set dimensions of hole using holyw and holyh
//hole needs to be correct size to frame samples neatly 
//set overall dimensions using wide and high


module holyblocker(wide=85, bolt=4, slot=8, high=200,holyw=40, holyh=100) {

difference() { //this is to cut sample hole
    union(){
    translate([0,0,0]) cube([wide,2,high]); //panel
        hull(){ //spine
    translate([wide/2,0,0]) cube([2,2,high]);
    translate([wide/2,0,0]) cube([2,slot+12,2]);
    }
translate([wide/2-1.2*holyw/2,0,high/2-1.1*holyh/2]) cube([1.2*holyw,5,1.1*holyh]);
            
    difference(){
translate([0,0,0]) cube([wide,slot+12,5]);        
    for (p=[slot: 3*slot: wide-slot]) {
        hull(){
translate([p,(slot+12)/2,-5]) cylinder(h=20,d=bolt);
translate([p+slot,(slot+12)/2,-5]) cylinder(h=20,d=bolt);
        }
        hull(){
translate([(p+2*slot),6,-5]) cylinder(h=20,d=bolt);
translate([(p+2*slot),slot+6,-5]) cylinder(h=20,d=bolt);
        }
    }
    }
}
translate([wide/2-holyw/2,0,high/2-holyh/2]) hull(){
    
translate([0,-0.1,0])    cube([holyw,1,holyh]);
translate([0,10,0])  minkowski(){  
    cube([holyw,1,holyh]); //base of subtracted
    sphere(r=5);//taper amount set by sphere diameter
}
}
}
}
//////////////////////END OF BLOCKER MODULES
